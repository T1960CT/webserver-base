package database

import (
	"github.com/alexbyk/panicif"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
)

// Create a message object
type Message struct {
	gorm.Model
	Value string `json:"value"`
}

// Create global go-orm database object
// gorm is a GoLang Object Relational Mapper,
// it handles mapping data objects and relational database tables
// and takes care of some of the tedious database stuff
var MessageDB *gorm.DB

// Create Base Message DB and initialize it for use
func MessageInit() error {
	var err error
	// This will open or create an sqlite3 database named "message"
	MessageDB, err = gorm.Open("sqlite3", "message.db")
	// If we cant create/open the database, panic, we cannot proceed
	panicif.Err(err)
	// Takes the structure of the Message object,
	// and automatically handles creating the table and columns
	MessageDB.AutoMigrate(&Message{})

	// Check if a default Message exists
	m := Message{}
	MessageDB.Find(&m)

	// If value isnt empty, it means
	// the db is already ready and there is no error
	if m.Value != "" {
		return nil
	}

	// Insert default record, returning how that went
	defaultMessage := Message{Value: "Default Server Message"}
	return MessageDB.Create(&defaultMessage).Error
}

// GetMessage finds and returns the stored Message object
func GetMessage() Message {
	// Create a Message object in memory to be populated later
	m := Message{}
	// Fill in the Message object
	MessageDB.Find(&m)
	return m
}

// UpdateMessage finds and updates the stored Message,
// and returns whether or not at least 1 row was affected
func UpdateMessage(newMessage map[string]interface{}) bool {
	// Create a Message object in memory to be populated later
	m := Message{}
	// Returns the outcome of the update being more than 1 (true),
	// or false if we didn't affect any rows, signifying update failed
	return MessageDB.First(&m).Update(newMessage).RowsAffected > 1
}
