# webserver-base

This project serves as a base for understanding some basic webserver concepts.

- GoLang for the Backend and Routing (main.go, api.go)
- SQLite3 for the Database (/database/db.go)
- HTML/CSS for frontend (/web)
- JavaScript for data exchange and animation (/web/scripts)

## Setup

Install Go from https://go.dev/ if you want to run from source

## Running

Simply run `go run *.go` to compile and run on the fly, or `go build`, which will compile to an executable for current platform

or using the prebuilt programs in `./compiled`
