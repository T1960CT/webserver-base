// This function will make a GET request to the API endpoint "message",
// Look for it in the route definitions in the GoLang server
function requestData() {
  fetch("http://localhost:8080/api/message", {
    method: "GET",
    // body: "",
    headers: { "Content-type": "application/json; charset=UTF-8" },
  }).then((response) => {
    // If the response code is not HTTP 200 ("Status OK") then there was a problem
    if (response.status !== 200) {
      console.error("Error. Status Code: " + response.status);
      return;
    }
    // Now we know the transaction was successful and we can process the response
    response
      // It's in JSON format, so we parse it on the fly
      .json()
      .then((data) => {
        // Now that we have the parsed data, load it into the page,
        document.getElementById("serverMessage").value = data["value"];
        // Check your console to see the JSON format response
        console.log("Received message: " + JSON.stringify(data));
      })
      // If there was a problem handling the response, print the error
      .catch((error) => console.error(error.message));
  });
}

// This function will make a POST request to the API endpoint "message",
// Look for it in the route definitions in the GoLang server
function submitData() {
  let message = document.getElementById("clientMessage").value;
  // Ensure we have contents to send
  if (message == "") {
    // If no data, show alert message in browser and dont send anything
    alert("Please define a message to send");
    return;
  }
  // Make it into JSON formatting before we send it
  let jsonMessage = { value: message };
  // Print what we intend to send to the console,
  // using JSON.stringify to print it as text instead of a JSON object
  console.log("Sending message: " + JSON.stringify(jsonMessage));

  // Send it to the server using a POST request to the api endpoint "message",
  // handling the responses accordingly
  fetch("http://localhost:8080/api/message", {
    method: "POST",
    body: JSON.stringify(jsonMessage),
    headers: { "Content-type": "application/json; charset=UTF-8" },
  })
    .then((response) =>
      alert("Successfully sent message to server. Press load to retrieve it!")
    )
    .catch((error) => alert("Error sending message: ", error));
}
