rm ./compiled/*

# Linux
env GOOS=linux GOARCH=386 go build -o ./compiled/webserver-base_linux_386
env GOOS=linux GOARCH=amd64 go build -o ./compiled/webserver-base_linux_amd64
env GOOS=linux GOARCH=arm go build -o ./compiled/webserver-base_linux_arm
env GOOS=linux GOARCH=arm64 go build -o ./compiled/webserver-base_linux_arm64
# Mac OSX
env GOOS=darwin GOARCH=arm go build -o ./compiled/webserver-base_darwin_arm
env GOOS=darwin GOARCH=arm64 go build -o ./compiled/webserver-base_darwin_arm64
# Windows 
env GOOS=windows GOARCH=386 go build -o ./compiled/webserver-base_windows_386
env GOOS=windows GOARCH=amd64 go build -o ./compiled/webserver-base_windows_amd64
env GOOS=windows GOARCH=arm go build -o ./compiled/webserver-base_windows_arm
env GOOS=windows GOARCH=arm64 go build -o ./compiled/webserver-base_windows_arm64


