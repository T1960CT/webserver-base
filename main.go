package main

import (
	"net/http"

	"github.com/alexbyk/panicif"
	"github.com/sirupsen/logrus"
	"gitlab.com/T1960CT/webserver-base/database"
)

// Create a variable we can easily change later if needed,
// without needing to dig around for everywhere it's used
const serverPort = ":8080"

// Init function, this is the first function to execute when running your code,
// and can be used to set things up before "the program itself" runs
func init() {
	// Get the message database prepared
	err := database.MessageInit()
	// If there was a problem, panic, we cannot continue
	panicif.Err(err)
}

// Main function, this is the first function to run after init
// Unless Main calls other functions, the end of the main function is the end of the program
func main() {
	// Using Logrus for prettier outputs, not necessary but better on the eyes and in debugging info
	logrus.Info("Starting server on port " + serverPort)

	// Start the webserver so it's ready on the port we specified, and uses the muxRouter to handle navigation
	if err := http.ListenAndServe(serverPort, buildAPI()); err != nil {
		// If there's a problem starting the server, exit program with error message
		logrus.Fatal(err)
	}
}
