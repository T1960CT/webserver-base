package main

import (
	"embed"
	"encoding/json"
	"fmt"
	"io/fs"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
	"gitlab.com/T1960CT/webserver-base/database"
)

//go:embed web
var webdir embed.FS

func handler() http.Handler {
	fsys := fs.FS(webdir)
	html, _ := fs.Sub(fsys, "web")
	return http.FileServer(http.FS(html))
}

func buildAPI() *mux.Router {
	// Create route handling so navigation is linked to functions when we run the server
	muxRouter := mux.NewRouter().StrictSlash(true)
	// When navigating to /ping, call the 'pong' function, limiting to only GET requests
	muxRouter.HandleFunc("/ping", pong).Methods("GET")

	// Set up the webserver that handles files and subdirectories
	muxRouter.PathPrefix("/scripts/").Handler(handler())
	muxRouter.PathPrefix("/styles/").Handler(handler())
	muxRouter.Handle("/", handler())

	// Set up a subrouter so we can isolate and group routes with this prefix
	apiRoutes := muxRouter.PathPrefix("/api").Subrouter()

	// Set up the API specific routes, see /web/scripts/client.js for the HTTP interactions
	apiRoutes.HandleFunc("/message", getMessage).Methods("GET")
	apiRoutes.HandleFunc("/message", setMessage).Methods("POST")

	return muxRouter
}

// The pong function is called when the user navigates to /ping, which is handled by the mux Router
func pong(w http.ResponseWriter, r *http.Request) {
	// Write the plain string data 'pong!' to the http.ResponseWriter instance "w",
	// which will handle passing it along to the user for us
	fmt.Fprintf(w, "pong!")
}

func getMessage(w http.ResponseWriter, r *http.Request) {
	// Tell the client the response will be in JSON formatting
	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	// Lazily retrieve the the message from database and send it to the client
	json.NewEncoder(w).Encode(database.GetMessage())
}

func setMessage(w http.ResponseWriter, r *http.Request) {
	// Create a key value object to decode data into, this is basically
	// "key": value (bool,int,string, anything)
	var m map[string]interface{}

	// Attempt to decode what was sent to us
	err := json.NewDecoder(r.Body).Decode(&m)
	if err != nil {
		// log the error
		logrus.Error(err)
		// Respond telling client there was an error
		w.WriteHeader(http.StatusBadRequest)
		// Write hand-made JSON bytes directly to client
		w.Write([]byte(`{ "message": "Error Unmarshaling JSON" }`))
		return
	}

	// Ensure database Message update was successful
	if database.UpdateMessage(m) {
		// Tell client all went well
		respJSON := []byte(`{ "message": "Message updated successfully" }`)
		w.WriteHeader(http.StatusOK)
		w.Write(respJSON)
		return
	}

	// If we've reached this point,
	// the database was not updated, ergo an error occured
	w.WriteHeader(http.StatusNotModified)
	w.Write([]byte(`{ "message": "Message could not be updated" }`))
}
