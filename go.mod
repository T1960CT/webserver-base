module gitlab.com/T1960CT/webserver-base

go 1.18

require (
	github.com/alexbyk/panicif v1.1.0
	github.com/gorilla/mux v1.8.0
	github.com/jinzhu/gorm v1.9.16
	github.com/sirupsen/logrus v1.9.0
)

require (
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/mattn/go-sqlite3 v1.14.16 // indirect
	golang.org/x/sys v0.0.0-20220715151400-c0bba94af5f8 // indirect
)
